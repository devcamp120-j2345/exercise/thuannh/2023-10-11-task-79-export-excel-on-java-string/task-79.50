package com.devcamp.customerorderproductcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorderproductcrud.model.Product;

public interface ProductRepo extends JpaRepository<Product, Integer> {

}
