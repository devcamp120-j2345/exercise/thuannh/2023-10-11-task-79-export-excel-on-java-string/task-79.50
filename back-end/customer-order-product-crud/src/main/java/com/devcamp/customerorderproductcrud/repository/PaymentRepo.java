package com.devcamp.customerorderproductcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorderproductcrud.model.Payment;

public interface PaymentRepo extends JpaRepository<Payment, Integer> {

}
